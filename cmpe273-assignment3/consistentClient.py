from flask import Flask, jsonify, json, request, Response
from ConsistentHashRing import ConsistentHashRing
from hash_ring import HashRing
import requests
app = Flask(__name__)

@app.route('/', methods = ["POST"])
def consistent_hash_repost():
    repo = json.loads(request.data)
    #print repo
    #start repost to consitent hash server

    #print long(md5.md5(repo['id']).hexdigest(), 16)
    #print hostClient[str(repo['id'])]

    #version one
    #url = ring.get_node(str(repo['id']))+'/v1/expenses'
    
    #version two
    url = hostClient[str(repo['id'])]+'/v1/expenses'
   
    print url
    headers = {'content-type': 'application/json'}
    response = requests.post(url, request.data, headers=headers)
    
    print response
    res = Response(status=201)
    return res

if __name__ == '__main__':
    #version one with hash_ring HashRing
    ''' memcache_servers = ['http://localhost:5000',
                    'http://localhost:5001',
                    'http://localhost:5002']
    weights = {
    'http://localhost:5000': 1,
    'http://localhost:5001': 2,
    'http://localhost:5002': 2
    }
    ring = HashRing(memcache_servers, weights)'''
    #version two with example provided by professor
    hostClient = ConsistentHashRing(3)
    #three manage system containers with port 5000, 5001, 5002
    hostClient['node0'] = 'http://localhost:5000'
    hostClient['node1'] = 'http://localhost:5001'
    hostClient['node2'] = 'http://localhost:5002'
    

    app.run(host="0.0.0.0", port=3000, debug=True)
